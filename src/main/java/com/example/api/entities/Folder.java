package com.example.api.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Folder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String folderIdentity;
    private String path;

    public Folder(String folderIdentity, String path) {
        this.folderIdentity = folderIdentity;
        this.path = path;
    }
}
