package com.example.api.controllers;

import com.example.api.commands.FolderAssetsCommand;
import com.example.api.commands.FolderCommand;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("")
public class FolderController {

    private List<FolderCommand> folderCommandList;
    private List<FolderAssetsCommand> folderAssetsList;

    public FolderController() throws IOException, ParseException {
        folderCommandList = new ArrayList<>();
        folderAssetsList = new ArrayList<>();

        InputStream inputStream = FolderController.class.getResourceAsStream("/json/data.json");
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(inputStream));
        for (Object key : jsonObject.keySet()) {
            String keyStr = (String) key;
            JSONObject data = (JSONObject) jsonObject.get(key.toString());
            folderCommandList.add(new FolderCommand(data.get("id").toString(), keyStr));
            folderAssetsList.add(new FolderAssetsCommand(data.get("id").toString(), data.get("assets").toString()));
        }
    }

    @GetMapping("/GET")
    public ResponseEntity<?> getAll() throws IOException {
        log.info("Getting list of folders...");
        return new ResponseEntity<>(folderCommandList,HttpStatus.OK);
    }

    @GetMapping("/GET/{folderId}")
    public ResponseEntity<FolderAssetsCommand> getById(@PathVariable("folderId") String folderId){
        log.info("Getting assets of folder...");
        Optional<FolderAssetsCommand> first = folderAssetsList.stream()
                .filter(element -> element.getId().equals(folderId))
                .findFirst();
        FolderAssetsCommand o = first.orElse(null);
        if (o==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(first.get(),HttpStatus.OK);
    }
}