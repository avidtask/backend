package com.example.api.Service;

import com.example.api.entities.Folder;

import java.util.Optional;

public interface FolderService {

    Iterable<Folder> findAll() ;
    Optional<Folder> findById(Long id);
    Folder save(Folder folder);
}
