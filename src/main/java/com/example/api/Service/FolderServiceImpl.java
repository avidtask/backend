package com.example.api.Service;

import com.example.api.controllers.FolderController;
import com.example.api.entities.Folder;
import com.example.api.repositories.FolderRepository;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

@Service
public class FolderServiceImpl implements FolderService{

    private FolderRepository folderRepository;

    public FolderServiceImpl(FolderRepository folderRepository) {
        this.folderRepository = folderRepository;
    }

    @Override
    public Iterable<Folder> findAll() {
        return folderRepository.findAll();
    }

    @Override
    public Optional<Folder> findById(Long id) {
        return folderRepository.findById(id);
    }

    @Override
    public Folder save(Folder folder) {
        return folderRepository.save(folder);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() throws IOException, ParseException {
        InputStream inputStream = FolderController.class.getResourceAsStream("/json/data.json");
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(inputStream));
        for (Object key : jsonObject.keySet()) {
            String keyStr = (String) key;
            JSONObject data = (JSONObject) jsonObject.get(key.toString());
            save(new Folder(data.get("id").toString(), keyStr));
        }
    }

}
